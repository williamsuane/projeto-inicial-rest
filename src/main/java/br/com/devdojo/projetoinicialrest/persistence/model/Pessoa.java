package br.com.devdojo.projetoinicialrest.persistence.model;

import javax.persistence.Entity;
import java.util.Objects;

/**
 * Created by William Suane on 11/15/2016.
 */
@Entity
public class Pessoa extends AbstractEntity {
    private String nome;

    public Pessoa() {
    }

    public Pessoa(String nome) {
        this.nome = nome;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }


    @Override
    public String toString() {
        return "Pessoa{" +
                "nome='" + nome + '\'' +
                '}';
    }
}
